package com.xnh.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * TODO
 *
 * @author xuannh
 * @description 请描述...
 * @date 2020-07-27 18:18
 */
@SpringBootApplication
@EnableScheduling
public class ScheduledMain8003 {

        public static void main(String[] args) {
              SpringApplication.run(ScheduledMain8003.class,args);
        }
}
