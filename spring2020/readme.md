[![Build Status](https://travis-ci.org/RutledgePaulV/spring2020.svg?branch=develop)](https://travis-ci.org/RutledgePaulV/spring2020)
[![Coverage Status](https://coveralls.io/repos/github/RutledgePaulV/spring2020/badge.svg?branch=develop)](https://coveralls.io/github/RutledgePaulV/spring2020?branch=develop)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.github.rutledgepaulv/spring2020/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.github.rutledgepaulv/spring2020)





Release Versions
```xml
<dependencies>
    <dependency>
        <groupId>com.xnh.springcloud</groupId>
        <artifactId>spring2020</artifactId>
        <version><!-- Not yet released --></version>
    </dependency>
</dependencies>
```

Snapshot Versions
```xml
<dependencies>
    <dependency>
        <groupId>com.xnh.springcloud</groupId>
        <artifactId>spring2020</artifactId>
        <version>1.0-SNAPSHOT</version>
    </dependency>
</dependencies>

<repositories>
    <repository>
        <id>ossrh</id>
        <name>Repository for snapshots</name>
        <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
    </repository>
</repositories>
```


This project is licensed under [MIT license](http://opensource.org/licenses/MIT).
