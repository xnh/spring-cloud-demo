/*
 * Copyright (c) 2001-2020 GuaHao.com Corporation Limited. All rights reserved.
 * This software is the confidential and proprietary information of GuaHao Company.
 * ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered into with GuaHao.com.
 */
package com.xnh.cloud;

import net.hasor.spring.boot.EnableHasor;
import net.hasor.spring.boot.EnableHasorWeb;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * TODO
 *
 * @author xuannh
 * @version V1.0
 * @since 2020-07-23 16:55
 */
@EnableHasor()
@EnableHasorWeb()
@SpringBootApplication(scanBasePackages = {"com.xnh.cloud.hasor","com.xnh.cloud.Controller"})
public class DataWayMain8002 {

  public static void main(String[] args) {
    SpringApplication.run(DataWayMain8002.class, args);
  }
}
