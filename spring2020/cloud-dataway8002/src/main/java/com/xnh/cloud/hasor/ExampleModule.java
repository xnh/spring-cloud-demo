/*
 * Copyright (c) 2001-2020 GuaHao.com Corporation Limited. All rights reserved.
 * This software is the confidential and proprietary information of GuaHao Company.
 * ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered into with GuaHao.com.
 */
package com.xnh.cloud.hasor;

import net.hasor.core.ApiBinder;
import net.hasor.core.DimModule;
import net.hasor.db.JdbcModule;
import net.hasor.db.Level;
import net.hasor.spring.SpringModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

/**
 * 实现
 *
 * @author xuannh
 * @version V1.0
 * @since 2020-07-23 17:13
 */
@DimModule
@Component
public class ExampleModule implements SpringModule {
  @Autowired private DataSource dataSource = null;

  @Override
  public void loadModule(ApiBinder apiBinder) throws Throwable {
    // .DataSource form Spring boot into Hasor
    apiBinder.installModule(new JdbcModule(Level.Full, this.dataSource));
  }
}
