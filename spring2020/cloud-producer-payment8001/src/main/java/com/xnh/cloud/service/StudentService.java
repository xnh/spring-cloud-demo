/*
 * Copyright (c) 2001-2020 GuaHao.com Corporation Limited. All rights reserved.
 * This software is the confidential and proprietary information of GuaHao Company.
 * ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered into with GuaHao.com.
 */
package com.xnh.cloud.service;

import com.xnh.cloud.common.utils.PageResult;
import com.xnh.cloud.entities.StudentDO;
import com.xnh.cloud.entities.StudentQuery;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * TODO
 *
 * @author xuannh
 * @version V1.0
 * @description 文件描述
 * @date 2020-07-30 15:03
 */
public interface StudentService {
	  
	  /**
	   * 新增
	   */
	  public int insert(@NotNull StudentDO student);
	  
	  /**
	   * 删除
	   */
	  public void delete(int id);
	  
	  /**
	   * 更新
	   */
	  public int update(@NotNull StudentDO student);
	  
	  /**
	   * 根据主键 id 查询
	   */
	  public StudentDO load(int id);
	  
	  /**
	   * 分页查询
	   */
	  public Map<String,Object> pageList(int pageno, int pagesize,StudentDO studentDO);

	/**
	 * 分页查询
	 */
	public PageResult<StudentDO> queryPage(StudentQuery query);
	
	/**
	 * 分页查询
	 */
	public List<StudentDO> queryList(int pageno, int pagesize);
}
