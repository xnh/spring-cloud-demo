package com.xnh.cloud.service.impl;

import cn.hutool.json.JSONUtil;
import com.xnh.cloud.common.utils.PageResult;
import com.xnh.cloud.dao.StudentDao;
import com.xnh.cloud.entities.StudentDO;
import com.xnh.cloud.entities.StudentQuery;
import com.xnh.cloud.service.StudentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TODO
 *
 * @author xuannh
 * @description 请描述...
 * @date 2020-07-30 16:17
 */
@Service
public class StudentServiceImpl implements StudentService {

  @Resource
  private StudentDao studentMapper;

  @Override
  public int insert(StudentDO student) {
    int ret = studentMapper.insert(student);
    return ret;
  }

  @Override
  public void delete(int id) {
    int ret = studentMapper.delete(id);
  }

  @Override
  public int update(StudentDO student) {
    int ret = studentMapper.update(student);
    return ret;
  }

  @Override
  public StudentDO load(int id) {
    return studentMapper.load(id);
  }

  @Override
  public Map<String, Object> pageList(int pageno, int pagesize,StudentDO studentDO) {
    List<StudentDO> pageList = studentMapper.pageList(pageno, pagesize,studentDO);
    int totalCount = studentMapper.pageListCount(pageno, pagesize);
    // result
    Map<String, Object> result = new HashMap<String, Object>();
    result.put("pageList", pageList);
    result.put("totalCount", totalCount);
    return result;
  }

  @Override
  public PageResult<StudentDO> queryPage(StudentQuery query) {
    PageResult<StudentDO> pageResult = new PageResult<>();
    StudentDO studentDO = new StudentDO();
    studentDO.setName(query.getName());
    System.out.println(JSONUtil.toJsonStr(studentMapper.pageList(query.getSqlRowStart(), query.getSqlRowEnd(),studentDO)));
    List<StudentDO> pageList = studentMapper.pageList(query.getSqlRowStart(), query.getSqlRowEnd(),studentDO);
    int totalCount = studentMapper.pageListCount(query.getSqlRowStart(), query.getSqlRowEnd());
    //分页查询
    List<StudentDO> list = studentMapper.queryPageList(query);
    pageResult.setList(pageList);
    pageResult.setPageNo(query.getPageNo());
    pageResult.setPageSize(query.getPageSize());
    pageResult.setPageTotal(totalCount);
    return pageResult;
  }

  @Override
  public List<StudentDO> queryList(int pageno, int pagesize) {
    return studentMapper.pageList(pageno, pagesize,new StudentDO());
  }
}
