package com.xnh.cloud.entities;

import lombok.Data;

import java.io.Serializable;

/**
 * TODO
 *
 * @author xuannh
 * @description 请描述...
 * @date 2020-07-30 14:23
 */
@Data
public class StudentDO implements Serializable {
	  
	  private static final long serialVersionUID = 1L;
	  
	  /**
	   * id
	   */
	  private Integer id;
	  
	  /**
	   * name
	   */
	  private String name;
	  
	  /**
	   * sex
	   */
	  private String sex;
	  
	  /**
	   * age
	   */
	  private Integer age;

}
