package com.xnh.cloud.controller;

import com.xnh.cloud.common.utils.PageResult;
import com.xnh.cloud.common.utils.Result;
import com.xnh.cloud.common.utils.ResultUtil;
import com.xnh.cloud.entities.StudentDO;
import com.xnh.cloud.entities.StudentQuery;
import com.xnh.cloud.service.StudentService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 学生信息查询服务
 * @author xuannh
 * @description 学生信息查询服务
 * @date 2020-07-30 16:25
 */
@RestController
@RequestMapping(value = "/student")
public class StudentController {
	  
	  @Resource
	  private StudentService studentService;
	
	/**
	 * [新增]
	 * @param student
	 * @return
	 */
	@RequestMapping("/insert")
	  public ResultUtil insert(StudentDO student){
			return ResultUtil.success(studentService.insert(student));
	  }
	
	/**
	 * [刪除]
	 * @param id
	 * @return
	 */
	@RequestMapping("/delete")
	  public ResultUtil delete(int id){
			 studentService.delete(id);
			return ResultUtil.success();
	  }
	
	/**
	 * [更新]
	 * @param student
	 * @return
	 */
	@RequestMapping("/update")
	  public ResultUtil update(StudentDO student){
			return ResultUtil.success(studentService.update(student));
	  }
	
	/**
	 * [查询] 根据主键 id 查询
	 * @param id
	 * @return
	 */
	@RequestMapping("/load")
	  public ResultUtil load(int id){
			return ResultUtil.success(studentService.load(id));
	  }
	
	/**
	 * [查询] 分页查询
	 * @param query
	 * @return
	 */
	@PostMapping("/pageList")
	  public Result<List<StudentDO>> pageList(@RequestBody StudentQuery query) {
			StudentDO studentDO = new StudentDO();
			studentDO.setName(query.getName());
			return Result.success(studentService.queryList(query.getPageNo(), query.getPageSize()));
	  }

	/**
	 * [查询] 分页查询 [带page参数]
	 * @param query
	 * @return
	 */
	@PostMapping("/queryPage")
	public Result<PageResult<StudentDO>> queryPage(@RequestBody StudentQuery query) {
		return Result.success(studentService.queryPage(query));
	}
}
