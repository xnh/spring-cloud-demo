/*
 * Copyright (c) 2001-2020 GuaHao.com Corporation Limited. All rights reserved.
 * This software is the confidential and proprietary information of GuaHao Company.
 * ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered into with GuaHao.com.
 */
package com.xnh.cloud;

import io.github.yedaxia.apidocs.Docs;
import io.github.yedaxia.apidocs.DocsConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.IOException;

/**
 * 支付服务启动类
 *
 * @author xuannh
 * @version V1.0
 * @since 2020-07-18 17:43
 */

@SpringBootApplication
public class PayMain8001 {
      public static void main(String[] args) {
          //生成本地apidoc接口文档
          apiDoc();
          SpringApplication.run(PayMain8001.class,args);
      }
    
    public static void apiDoc(){
        DocsConfig config = new DocsConfig();
        // 参数为空、自动获取项目根目录
        File directory = new File("");
        String courseFile = "";
        try {
            courseFile = directory.getCanonicalPath() + "/cloud-producer-payment8001";
        } catch (IOException e) {
            e.printStackTrace();
        }
        config.setProjectPath(courseFile); // 当前项目根目录
        config.setProjectName("Student Service"); // 项目名称
        config.setApiVersion("V1.0");       // 声明该API的版本
        config.setDocsPath(courseFile + "/src/main/resources/apidoc"); // 生成API 文档所在目录
        config.setAutoGenerate(Boolean.TRUE);  // 配置自动生成
        Docs.buildHtmlDocs(config); // 执行生成文档
    }
}
