package com.xnh.cloud.common.utils;

import lombok.Data;

/**
 * 公共分页参数
 *
 * @author xuannh
 * @description 分页请求公共参数
 * @date 2020-08-13 15:56
 */
@Data
public class Page {
	private int pageNo = 1;
	private int pageSize = 10;
	private int sqlRowStart;
	private int sqlRowEnd;
	public int getSqlRowStart() {
		sqlRowStart = (pageNo - 1) * pageSize;
		return sqlRowStart;
	}
	public int getSqlRowEnd() {
		sqlRowEnd = pageSize;
		return sqlRowEnd;
	}
	public int getPageNo() {
		return pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
}
