/*
 * Copyright (c) 2001-2020 GuaHao.com Corporation Limited. All rights reserved.
 * This software is the confidential and proprietary information of GuaHao Company.
 * ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered into with GuaHao.com.
 */
package com.xnh.cloud.dao;

import com.xnh.cloud.entities.StudentDO;
import com.xnh.cloud.entities.StudentQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 学生信息服务
 *
 * @author xuannh
 * @version V1.0
 * @description 文件描述
 * @date 2020-07-30 14:54
 */
@Mapper
public interface StudentDao {
	  
	  /**
	   * [新增]
	   * @date 2020/07/30
	   **/
	  int insert(StudentDO student);
	  
	  /**
	   * [刪除]
	   * @date 2020/07/30
	   **/
	  int delete(int id);
	  
	  /**
	   * [更新]
	   * @date 2020/07/30
	   **/
	  int update(StudentDO student);
	  
	  /**
	   * [查询] 根据主键 id 查询
	   * @date 2020/07/30
	   **/
	  StudentDO load(int id);
	  
	  /**
	   * [查询] 分页查询
	   * @date 2020/07/30
	   **/
	  List<StudentDO> pageList(@Param("sqlRowStart")int pageno,@Param("sqlRowEnd")int pagesize,@Param("pm") StudentDO studentDO);

	/**
	 * [查询] 分页查询
	 * @param query
	 * @return
	 */
	List<StudentDO> queryPageList(StudentQuery query);

	  /**
	   * [查询] 分页查询 count
	   * @date 2020/07/30
	   **/
	  int pageListCount(@Param("sqlRowStart")int pageno,@Param("sqlRowEnd")int pagesize);
	  
}
