package com.xnh.cloud.common.utils;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页工具
 *
 * @author xuannh
 * @description 请描述...
 * @date 2020-07-31 10:03
 */
@Data
public class PageUtil implements Serializable {
  /**
   * 页码
   */
  private int offset = 1;
  /**
   * 每页显示条数
   */
  private int pagesize = 10;
  
  private int pageno = 1;
  
  private int pagecnt = 0;
  private int total = 0;

  private Object data;

  private List<?> records;

  PageUtil() {}

  PageUtil(int offset, int pagesize, int total, Object data) {
    this.offset = offset;
    this.pagesize = pagesize;
    this.total = total;
    this.pagecnt =
        this.total % this.pagesize == 0
            ? this.total / this.pagesize
            : this.total / this.pagesize + 1;
    this.data = data;
  }

  PageUtil(int offset, int pagesize, int total, List records) {
    this.offset = offset;
    this.pagesize = pagesize;
    this.total = total;
    this.pagecnt =
        this.total % this.pagesize == 0
            ? this.total / this.pagesize
            : this.total / this.pagesize + 1;
    this.records = records;
  }

  public int getOffset() {
    return (offset - 1) * pagesize;
  }
  
  public int getPageno() {
    return (pageno - 1) * pagesize;
  }
  
  public int getPagesize() {
    return pagesize;
  }
}
