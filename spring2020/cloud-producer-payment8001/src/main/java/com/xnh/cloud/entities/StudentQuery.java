package com.xnh.cloud.entities;

import com.xnh.cloud.common.utils.Page;
import lombok.Data;

/**
 * TODO
 *
 * @author xuannh
 * @description 请描述...
 * @date 2020-08-13 15:59
 */
@Data
public class StudentQuery extends Page {
	
	/**
	 * name
	 */
	private String name;
	
	/**
	 * sex
	 */
	private String sex;
	
}
