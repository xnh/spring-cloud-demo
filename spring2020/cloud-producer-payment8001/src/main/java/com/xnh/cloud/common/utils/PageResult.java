package com.xnh.cloud.common.utils;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PageResult<T> implements Serializable {

    private int pageNo;
    private int pageSize;
    private int pageTotal;
    private List<T> list;

    public PageResult(){}

    PageResult(int pageNo,int pageSize,int pageTotal,List<T> list){
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.pageTotal = pageTotal;
        this.list = list;
    }


}
