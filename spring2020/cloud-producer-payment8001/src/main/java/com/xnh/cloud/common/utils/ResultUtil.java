package com.xnh.cloud.common.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;
import java.util.List;

/**
 * 返回工具类
 *
 * @author xuannh
 * @description 请描述...
 * @date 2020-07-30 15:29
 */
public class ResultUtil implements Serializable {

  // 定义jackson对象
  private static final ObjectMapper MAPPER = new ObjectMapper();

  // 响应业务状态
  private Integer code;

  // 响应消息
  private String msg;

  // 响应中的数据
  private Object data;

  public ResultUtil() {}

  public ResultUtil(Integer code, String msg, Object data) {
    this.code = code;
    this.msg = msg;
    this.data = data;
  }

  public ResultUtil(Object data) {
    this.code = 200;
    this.msg = "OK";
    this.data = data;
  }
  
  public static ResultUtil build(Integer code, String msg, Object data) {
    return new ResultUtil(code, msg, data);
  }

  public static ResultUtil success(Object data) {
    return new ResultUtil(data);
  }

  public static ResultUtil success() {
    return new ResultUtil(null);
  }

  public static ResultUtil build(Integer code, String msg) {
    return new ResultUtil(code, msg, null);
  }
  
  public static ResultUtil error(String msg) {
    return new ResultUtil(400, msg, null);
  }

  /**
   * 将json结果集转化为Result对象
   *
   * @param jsonData json数据 传的是Result的对象的Json字符串
   * @param clazz TaotaoResult中的object类型
   * @return
   */
  public static ResultUtil formatToPojo(String jsonData, Class<?> clazz) {
    try {
      if (clazz == null) {
        return MAPPER.readValue(jsonData, ResultUtil.class);
      }
      JsonNode jsonNode = MAPPER.readTree(jsonData);
      JsonNode data = jsonNode.get("data");
      Object obj = null;
      if (clazz != null) {
        if (data.isObject()) {
          obj = MAPPER.readValue(data.traverse(), clazz);
        } else if (data.isTextual()) {
          obj = MAPPER.readValue(data.asText(), clazz);
        }
      }
      return build(jsonNode.get("code").intValue(), jsonNode.get("msg").asText(), obj);
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * 没有object对象的转化
   *
   * @param json
   * @return
   */
  public static ResultUtil format(String json) {
    try {
      return MAPPER.readValue(json, ResultUtil.class);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  /**
   * Object是集合转化
   *
   * @param jsonData 传的是Result的对象的Json字符串 json数据
   * @param clazz 集合中的类型
   * @return
   */
  public static ResultUtil formatToList(String jsonData, Class<?> clazz) {
    try {
      JsonNode jsonNode = MAPPER.readTree(jsonData);
      JsonNode data = jsonNode.get("data");
      Object obj = null;
      if (data.isArray() && data.size() > 0) {
        obj =
            MAPPER.readValue(
                data.traverse(),
                MAPPER.getTypeFactory().constructCollectionType(List.class, clazz));
      }
      return build(jsonNode.get("code").intValue(), jsonNode.get("msg").asText(), obj);
    } catch (Exception e) {
      return null;
    }
  }

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer status) {
    this.code = status;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }
}
